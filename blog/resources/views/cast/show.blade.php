@extends('layout.master')

@section('judul')
Detail Pemain {{$cast->nama}}
@endsection

@section('content')

<h1>{{$cast->nama}}</h1>
<p>{{$cast->deskripsi}}</p>

@endsection